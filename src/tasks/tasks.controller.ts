import { Controller, Delete, Get, Patch, Post, Put } from '@nestjs/common';
import { TasksService } from './tasks.service';

@Controller({ path: '/tasks' })
export class TasksController {
  constructor(private taskService: TasksService) {}

  @Get()
  getAllTasks() {
    return this.taskService.getTasks();
  }

  @Post()
  createTask() {
    return { message: 'creating tasks ' };
  }
  @Put()
  updateTask() {
    return { message: 'updating tasks ' };
  }

  @Delete()
  deleteTask() {
    return { message: 'deleting tasks ' };
  }

  @Patch()
  patchTask() {
    return { message: 'patching tasks ' };
  }
}
