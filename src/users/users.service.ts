import { Injectable } from '@nestjs/common';

@Injectable()
export class UsersService {
  private users = [
    {
      id: 1,
      name: 'Jhon Doe',
      phone: '1234566778',
    },
    {
      id: 2,
      name: 'Jane Doe',
      phone: '1234566778',
    },
  ];
  getUsers() {
    return this.users;
  }
}
